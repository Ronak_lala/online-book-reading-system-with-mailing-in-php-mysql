<!DOCTYPE html>
<html lang="en">
<?php
error_reporting(0);
@ini_set('display_errors', 0);
session_start();
if (!isset ($_GET['msg']) ) {  
    $page = '';  
} else {  
    $page = "Incorrect Username/Password";
}  
$name = $_SESSION['uname'];

if(isset($name)) {
    header("location:index.php");
}
?>
<head>
    <meta charset="UTF-8">
    <title>Book Store</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#03a6f3">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/styles.css">
</head>

<body>
    <header>
       
        <div class="main-menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="navbar-item">
                                <a href="index.html" class="nav-link">Home</a>
                            </li>
                            <li class="navbar-item">
                                <a href="books.php" class="nav-link">Read</a>
                            </li>
                            <li class="navbar-item">
                                <a href="about" class="nav-link">About</a>
                            </li>
                            <li class="navbar-item active">
                                <a href="#" class="nav-link">Login / Signup</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <div class="breadcrumb">
        <div class="container">
            <a class="breadcrumb-item" href="index.html">Home</a>
            <span class="breadcrumb-item active">Login</span>
        </div>
    </div>
    <section class="static about-sec">
        <div class="container">
            <h1>My Account / Login</h1>
            <p>Fairy tales are more than true not because they tell us that dragons exist, but because they tell us that dragons can be beaten. </p>
            <div class="form">
                <form action = "log.php" method="POST">
                    <div class="row">
                        <div class="col-md-4">
                            <input placeholder="Enter User Name" name="uname" required>
                            <span class="required-star">*</span>
                        </div>
                        <div class="col-md-4" id="showhide">
                            <input type="password" name="pass" id="password" placeholder="Enter the password">
       
                        </div>
 <i class="far fa-eye" id="togglePassword"></i>
<style type="text/css">
    #togglePassword {
    margin-left: -50px;
    cursor: pointer;
    position: relative;
    z-index: 3;
    margin-top:15px;
}
</style>       
<script type="text/javascript">
    const togglePassword = document.querySelector('#togglePassword');
const password = document.querySelector('#password');
togglePassword.addEventListener('click', function (e) {
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    this.classList.toggle('fa-eye-slash');
});
</script>         

                        <div class="col-lg-8 col-md-12">
                            <p style = "color:red;font-weight: bold;float:left;"><?php echo $page;?></p>

                            <input type = "submit" class="btn black" value = "Login">
                            <h5> <a style="color: #93A09E;" href="forgotpass.php">forgot password?</a></h5>
                            <br><br>
                            <h5>not Registered? <a href="register.php">Register here</a></h5>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <footer>
         <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="address">
                        <h4>Our Address</h4>
                        <h6>flat no.204,Aravalli building,Dipti sky city</h6>
                        <h6>Call : 9511962448</h6>
                        <h6>Email : infobookstore3@gmail.com</h6>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="navigation">
                        <h4>Navigation</h4>
                        <ul>
                            <li><a href="index">Home</a></li>
                            <li><a href="about">About Us</a></li>
                            <li><a href="privacy-policy">Privacy Policy</a></li>
                            <li><a href="terms-conditions">Terms</a></li>
                        </ul>
                    </div>
                    <div class="navigation">
                        <h4>Help</h4>
                        <ul>
                            <li><a href="privacy-policy">Privacy</a></li>
                            <li><a href="faq">FAQ’s</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form">
                        <h3>Quick Contact us</h3>
                        <h6>If you have any problem, you can leave a message</h6>
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <input placeholder="Name" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="email" placeholder="Email" required>
                                </div>
                                <div class="col-md-12">
                                    <textarea placeholder="Messege"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn black">Alright, Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h5>(C) 2021. All Rights Reserved. BookStore</h5>
                    </div>
                    <div class="col-md-6">
                        <div class="share align-middle">
                            <a href="https://www.facebook.com/abhishek.nagari.3/"><span class="fb"><i class="fa fa-facebook-official"></i></span></a>
                           <a href="https://www.instagram.com/mr._.awesome._/"><span class="instagram"><i class="fa fa-instagram"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>