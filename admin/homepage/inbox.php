<?php
session_start();
$name = $_SESSION['username'];
include '../dbconn.php';
$a = "SELECT * FROM admin WHERE username = '{$name}'";
$r = mysqli_query($conn,$a);
$o = mysqli_fetch_assoc($r);
$aid = $o['ID'];

if (!isset ($_GET['imp']) ) {  
    $imp = "";  
} else {  
    $imp = $_GET['imp'];  
} 
?>

<!DOCTYPE HTML>
<html>
<head>
<title>Inbox</title>
<script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Augment Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/amcharts.js"></script>	
<script src="js/serial.js"></script>	
<script src="js/light.js"></script>	
<script src="js/radar.js"></script>	
<link href="css/barChart.css" rel='stylesheet' type='text/css' />
<link href="css/fabochart.css" rel='stylesheet' type='text/css' />
<!--clock init-->
<script src="js/css3clock.js"></script>
<!--Easy Pie Chart-->
<!--skycons-icons-->
<script src="js/skycons.js"></script>
<!--//skycons-icons-->
</head> 
<body>
   <div class="page-container">
   <!--/content-inner-->
	<div class="left-content">
	   <div class="inner-content">
		<!-- header-starts -->
			<div class="header-section">
						<!--menu-right-->
						<div class="top_menu">
									<script type="text/javascript">
										 $('.main-search').hide();
										$('button').click(function (){
											$('.main-search').show();
											$('.main-search text').focus();
										}
										);
										$('.close').click(function(){
											$('.main-search').hide();
										});
									</script>
							<!--/profile_details-->
								<div class="profile_details_left">
									<ul class="nofitications-dropdown">
											 </li>
									      <?php
												$sql = "SELECT * FROM contact order by ID DESC limit 3";
                                                           $res = mysqli_query($conn,$sql);
                                                           $count = mysqli_num_rows($res);

												?>
										<li class="dropdown note">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope-o"></i> <span class="badge"><?php echo $count; ?></span></a>
												<?php
function time_ago_in_php($timestamp){
  
  date_default_timezone_set("Asia/Kolkata");         
  $time_ago        = strtotime($timestamp);
  $current_time    = time();
  $time_difference = $current_time - $time_ago;
  $seconds         = $time_difference;
  
  $minutes = round($seconds / 60); 
  $hours   = round($seconds / 3600);
  $days    = round($seconds / 86400);   
  $weeks   = round($seconds / 604800);  
  $months  = round($seconds / 2629440);  
  $years   = round($seconds / 31553280); 
                
  if ($seconds <= 60){

    return "Just Now";

  } else if ($minutes <= 60){

    if ($minutes == 1){

      return "one minute ago";

    } else {

      return "$minutes minutes ago";

    }

  } else if ($hours <= 24){

    if ($hours == 1){

      return "an hour ago";

    } else {

      return "$hours hrs ago";

    }

  } else if ($days <= 7){

    if ($days == 1){

      return "yesterday";

    } else {

      return "$days days ago";

    }

  } else if ($weeks <= 4.3){

    if ($weeks == 1){

      return "a week ago";

    } else {

      return "$weeks weeks ago";

    }

  } else if ($months <= 12){

    if ($months == 1){

      return "a month ago";

    } else {

      return "$months months ago";

    }

  } else {
    
    if ($years == 1){

      return "one year ago";

    } else {

      return "$years years ago";

    }
  }
}
												?>
												<?php
												$sql = "SELECT * FROM contact order by ID DESC limit 3";
                                                           $res = mysqli_query($conn,$sql);
                                                           $count = mysqli_num_rows($res);

												?>
													<ul class="dropdown-menu two first">
														<li>
															<div class="notification_header">
																<h3>You have <?php echo $count;?> new messages  </h3> 
															</div>
														</li>
														<?php 
                                                           
                                                           while($row = mysqli_fetch_assoc($res)) {
                                                           	$time = $row['dates'];
                                                           	?>
                                                           	<li><a href="#">
														   <div class="user_img"><img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png" alt=""></div>
														   <div class="notification_desc">
															<p><?php echo $row['name']; ?> Contacted You</p>
															<p><span><?php echo time_ago_in_php($time); ?></span></p>
															</div>
														   <div class="clearfix"></div>	
														 </a></li>
                                                           	<?php
                                                           }
														?>
														
														
														 
															<div class="notification_bottom">
																<a href="inbox.php">See all messages</a>
															</div> 
														</li>
													</ul>
										</li>
										<?php
$asql = "SELECT * FROM users order by ID DESC limit 5";
$ares = mysqli_query($conn,$asql);
$count = mysqli_num_rows($ares);
?>
							<li class="dropdown note">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell-o"></i> <span class="badge"><?php echo $count; ?></span></a>

									<ul class="dropdown-menu two">
										<li>
											<div class="notification_header">
												<h3>You have <?php echo $count; ?> new user on BookStore</h3>
											</div>
										</li>
										<?php
										while($arow = mysqli_fetch_assoc($ares)) {
?>
<li><a href="#">
											<div class="user_img"><img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png" alt=""></div>
										   <div class="notification_desc">
											<p><?php echo $arow['uname']; ?> Has Registered</p>
											</div>
										  <div class="clearfix"></div>	
										 </a></li>
<?php
}
										?>
										
										
									</ul>
							</li>	
							<li class="dropdown note">
								<a href="../../login.php" class="dropdown-toggle"  aria-expanded="false"><i class="fa fa-home"></i> </a>
							</li>
									 							   		
							<div class="clearfix"></div>	
								</ul>
							</div>
							<div class="clearfix"></div>	
							<!--//profile_details-->
						</div>
						<!--//menu-right-->
					<div class="clearfix"></div>
				</div>
					<!-- //header-ends -->
							<!--//outer-wp-->
								<div class="outter-wp">
								<!--/sub-heard-part-->
											 <div class="sub-heard-part">
													   <ol class="breadcrumb m-b-0">
															<li><a href="index.php">Home</a></li>
															<li class="active">Inbox</li>
														</ol>
											</div>	
											 <div class="sub-heard-part" style="font-weight: bold;color: green;">
											 	<?php echo $imp; ?>
											 </div>
										<!--/sub-heard-part-->		
									<!--/inbox-->
									 <div class="inbox-mail">
									<div class="col-md-4 compose">
										<form action="#" method="GET">
												<!-- Input Group -->
											</form>
											<a href = "compose.php"><h2>Compose</h2></a>
														<nav class="nav-sidebar">
															<?php
												$sql = "SELECT * FROM contact WHERE areplied = 0 AND deleted = 0 order by ID DESC";
                                                           $res = mysqli_query($conn,$sql);
                                                           $count = mysqli_num_rows($res);

												?>
												<?php
												$sql = "SELECT * FROM contact WHERE deleted = 1 order by ID DESC";
                                                           $res = mysqli_query($conn,$sql);
                                                           $countd = mysqli_num_rows($res);

												?>
												<?php
												$sql = "SELECT * FROM reply WHERE aid = '{$aid}'";
                                                           $res = mysqli_query($conn,$sql);
                                                           $counta = mysqli_num_rows($res);

												?>
                        <?php
                        $sql = "SELECT * FROM composed WHERE aid = '{$aid}'";
                                                           $res = mysqli_query($conn,$sql);
                                                           $countc = mysqli_num_rows($res);

                        ?>
												<?php
												$counter = 0;
												$sql = "SELECT * FROM contact WHERE imp = 1 AND deleted = 0";
												$res = mysqli_query($conn,$sql);
												while ($row = mysqli_fetch_assoc($res)) {
													$id = $row['ID'];
													$newsql = "SELECT * FROM impmail where mid = '{$id}' AND aid = '{$aid}'";
													$newres = mysqli_query($conn,$newsql);
													while ($newr = mysqli_fetch_assoc($newres)) {
														$counter = $counter + 1 ; 
													}
												}

												?>
														<ul class="nav tabs">
														  <li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-inbox"></i>Inbox <span><?php echo $count; ?></span><div class="clearfix"></div></a></li>
														  <li class=""><a href="#tab2" data-toggle="tab"><i class="fa fa-reply-all" aria-hidden="true"></i> Replied <span><?php echo $counta; ?></span></a></li>
														  <li class=""><a href="#tab3" data-toggle="tab"><i class="fa fa-star-o"></i>Important <span><?php echo $counter; ?></span></a></li> 
														  
														  <li class=""><a href="#tab5" data-toggle="tab"><i class="fa fa-trash-o"></i>Delete <span><?php echo $countd; ?></span></a></li>                     <li class=""><a href="#tab6" data-toggle="tab"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Composed<span><?php echo $countc; ?></span></a></li>          
														</ul>
													</nav>
																
														</div>
														<!-- tab content -->
														<div class="col-md-8 tab-content tab-content-in">
														<div class="tab-pane active text-style" id="tab1">
														  <div class="inbox-right">
																	
												<div class="mailbox-content">
                <table class="table">
                    <tbody><?php 
                                                     $newsql = "SELECT * FROM contact WHERE deleted = 0 order by ID DESC";
                                                     $newres = mysqli_query($conn,$newsql);      
                                                           while($nrow = mysqli_fetch_assoc($newres)) {
                                                           	$time = $nrow['dates'];
                                                           	$areplied = $nrow['areplied'];
                                                           	$imp = $nrow['imp'];
                                                           	?>
                    	
                        <tr class="table-row">
                            <td class="table-img">
                           <img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png" alt="" height="50" width="50" />
                            </td>
                            <td class="table-text">
                            	<h6> <?php echo $nrow['name']; ?></h6>
                                <p><?php echo substr($nrow['message'],0,100);?></p>
                            </td>
                            <td>
                            	<?php
                              if($areplied == 0) {
                              	?>
                              	<td>
                              	<a href = 'areply.php?uid=<?php echo $nrow["ID"]; ?>'><span class="fam">reply</span></a>
                              </td>
                              <td>
                            	<a href = 'adel.php?uid=<?php echo $nrow["ID"]; ?>'><span class="ur">Delete</span></a>
                            </td>
                              	<?php
                              }
                              else {
                              	?>
                              	<td>
                              	<a href = '#'><span class="mar">replied</span></a>
                              </td>
                              <td>
                            	<a href = '#'><span></span></a>
                            </td>
                              	<?php
                              }
                            	?>
                            	
                            </td>
                            
                            <td class="march">
                              <?php echo time_ago_in_php($time); ?>
                            </td>
                          
                             <td>
                         <?php
                         if($imp == 1) {
                         	?>
                         	<a href="setimp.php?id=<?php echo $nrow['ID']; ?>"> <i  class="fa fa-check" id = "setimp"></i></a>
                         	<?php
                         }
                         else {
                         	?>
                         	<a href="setimp.php?id=<?php echo $nrow['ID']; ?>"> <i  class="fa fa-star-half-o icon-state-warning" id = "setimp"></i></a>
                         	<?php
                         }
                         ?>
							
                            </td>
                        </tr>
                       <?php
}
                       ?>
                    </tbody>
                </table>
               </div>
            </div>
</div>
<div class="tab-pane text-style" id="tab2">
	<div class="inbox-right">
         	
            <div class="mailbox-content">
            	<!-- sent-->
                <table class="table">
                    <tbody>
                    
                       <?php 
                                                     $newsql = "SELECT * FROM reply WHERE aid = '{$aid}' order by ID DESC";
                                                     $newres = mysqli_query($conn,$newsql);      
                                                           while($nrow = mysqli_fetch_assoc($newres)) {
                                                           	$dtime = $nrow['dtime'];
                                                           	$rid = $nrow['rid'];
                                                           	$newsql = "SELECT * FROM contact WHERE ID = '{$rid}'";
                                                           	$res = mysqli_query($conn,$newsql);
                                                           	$ncrows = mysqli_fetch_assoc($res);
                                                           	?>
                    	
                        <tr class="table-row">
                            <td class="table-img">
                            	
                           <img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png" alt="" height="50" width="50" />
                            </td>
                            <td class="table-text">
                            	<h6 style="text-align: left;"><?php echo $ncrows['name']; ?></h6>
                                <p style="text-align: left;"><?php echo substr($nrow['areply'],0,100);?></p>
                            </td>
                            <td class="march">
                              <?php echo time_ago_in_php($dtime); ?>
                            </td>
                             <td>
                                
                            </td>
                        </tr>
                       <?php
}
                       ?>
                       
                      
                    </tbody>
                </table>
               </div>
            </div>
</div>
<div class="tab-pane text-style" id="tab3">
	<div class="inbox-right">
         	
            <div class="mailbox-content">
            <!-- important-->
                <table class="table">
                    <tbody>
                        
                      <?php 
                                                     $newsql = "SELECT * FROM impmail WHERE aid = '{$aid}' order by ID DESC";
                                                     $newres = mysqli_query($conn,$newsql);      
                                                           while($nrow = mysqli_fetch_assoc($newres)) {
                                                           $mid = $nrow['mid'];
                                                           $i = "SELECT * FROM contact WHERE ID = '$mid' AND deleted  = 0";
                                                           $j = mysqli_query($conn,$i);
                                                           while($k = mysqli_fetch_assoc($j)) {


                                                           	?>
                    	
                        <tr class="table-row">
                            <td class="table-img">
                            	
                           <img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png" alt="" height="50" width="50" />
                            </td>
                            <td class="table-text">
                            	<h6 style="text-align: left;"><?php echo $k['name']; ?></h6>
                                <p style="text-align: left;"><?php echo substr($k['message'],0,100);?></p>
                            </td>
                            <td class="march">
                              
                            </td>
                             <td>
                                
                            </td>
                        </tr>
                       <?php
                   }
}
                       ?>
                       
                    </tbody>
                </table>
               </div>
            </div>
 </div>
 
  <div class="tab-pane text-style" id="tab5">
  	<div class="inbox-right">
         	
           
                <table class="table">

                    <tbody>
                    	<tr>
                    		<div class="mailbox-content">
<!--delete--><div class="float-right">
	<?php
$newsql = "SELECT * FROM contact WHERE deleted = 1 order by ID DESC";
                                                     $newres = mysqli_query($conn,$newsql);      
                                                     $newcount = mysqli_num_rows($newres);
                                                     if($newcount > 0) {
	?>
	<a href = "emptyt.php"><button style="border:none;background-color: transparent;border:1px solid grey;height: 35px;
	width: auto;border-radius: 0px;">Empty Trash&nbsp;&nbsp;&nbsp;<i class="fa fa-eraser" aria-hidden="true"></i></button></a>
</div>
                    	</tr><br><br>
                       
                       <?php 
                                                     
                                                           while($nrow = mysqli_fetch_assoc($newres)) {
                                                         


                                                           	?>
                    	
                        <tr class="table-row">
                            <td class="table-img">
                            	
                           <img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png" alt="" height="50" width="50" />
                            </td>
                            <td class="table-text">
                            	<h6 style="text-align: left;"><?php echo $nrow['name']; ?></h6>
                                <p style="text-align: left;"><?php echo substr($nrow['message'],0,100);?></p>
                            </td>
                            <td class="march">
                              
                            </td>
                             <td>
                                <a href="mres.php?id=<?php echo $nrow['ID']; ?>"> <i class="fa fa-undo" aria-hidden="true"></i></a>
                            </td>
                            <td>
                                <a href="mdel.php?id=<?php echo $nrow['ID']; ?>"> <i  class="fa fa-trash-o" id = "setimp"></i></a>
                            </td>
                        </tr>
                       <?php
                  
}
}
else {
	?>
	<tr>
	<td><center><h1>No Mails in Trash</h1></center></td>
	<tr>
	<?php
}
                       ?>
                       
                       
                        
															</tbody>
														</table>
													   </div>
													</div>
						<div class="tab-pane text-style" id="tab6">
  <div class="inbox-right">
          
            <div class="mailbox-content">
            <!-- important-->
                <table class="table">
                    <tbody>
                        
                      <?php 
                                                     $newsql = "SELECT * FROM composed WHERE aid = '{$aid}' order by ID DESC";
                                                     $newres = mysqli_query($conn,$newsql);      
                                                           while($nrow = mysqli_fetch_assoc($newres)) {
                                                            $sqls = "SELECT * FROM admin WHERE ID = '{$aid}'";
                                                            $ress = mysqli_query($conn,$sqls);
                                                            while ($rowss = mysqli_fetch_assoc($ress)) {
                                                              # code...
                                                            
                                                            ?>
                      
                        <tr class="table-row">
                            <td class="table-img">
                              
                           <img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png" alt="" height="50" width="50" />
                            </td>
                            <td class="table-text">
                              <h6 style="text-align: left;"><?php echo $nrow['email']; ?></h6>
                                <p style="text-align: left;"><?php echo substr($nrow['msg'],0,100);?></p>
                            </td>
                            <td class="march">
                              
                            </td>
                             <td>
                                
                            </td>
                        </tr>
                       <?php
          }         
}
                       ?>
                       
                    </tbody>
                </table>
               </div>
            </div>
 </div>
										<div class="clearfix"> </div>
										   </div>
											
										</div>

								<!--//outer-wp-->
								<!--footer section start-->
										<footer>
										   <p>&copy 2021 . All Rights Reserved Bookstore</a></p>
										</footer>
								<!--footer section end-->
							</div>
						</div>
				<!--//content-inner-->
			<!--/sidebar-menu-->
					<div class="sidebar-menu">
					<header class="logo">
					<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="index.php"> <span id="logo"> <h1>Bookstore</h1></span> 
					<!--<img id="logo" src="" alt="Logo"/>--> 
				  </a> 
				</header>
			<div style="border-top:1px solid rgba(69, 74, 84, 0.7)"></div>
			<!--/down-->
							<div class="down">	
									  <a href="profile"><img src="https://static.thenounproject.com/png/17241-200.png" height="80" width="80"></a>
									  <a href="profile"><span class=" name-caret"><?php echo $name; ?></span></a>
									 <p>System Administrator in Company</p>
									<ul>
									<li><a class="tooltips" href="profile.php"><span>Profile</span><i class="lnr lnr-user"></i></a></li>
										<li><a class="tooltips" href="logout"><span>Log out</span><i class="lnr lnr-power-switch"></i></a></li>
										</ul>
									</div>
							   <!--//down-->
                           <div class="menu">
									<ul id="menu" >
									<li><a href="index"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
										 <li id="menu-academico" ><a href="#"> <i class="fa fa-book"></i> <span>Books</span> <span class="fa fa-angle-right" style="float: right"></span></a>
											 <ul id="menu-academico-sub" >
												<li id="menu-academico-avaliacoes" ><a href="vb.php">View / Delete Books</a></li>
												<li id="menu-academico-boletim" ><a href="ab.php">Add Books</a></li>
											  </ul>
										 </li>
									 <li><a href="#"><i class="lnr lnr-envelope"></i> <span>Mail Box</span><span class="fa fa-angle-right" style="float: right"></span></a>
									   <ul>
										<li><a href="inbox.php"><i class="fa fa-inbox"></i> Inbox</a></li>
										<li><a href="compose.php"><i class="fa fa-pencil-square-o"></i> Compose Mail</a></li>
									
									  </ul>
									</li>
								  </ul>
								</div>
							  </div>
							  <div class="clearfix"></div>		
							</div>
							<script>
							var toggle = true;
										
							$(".sidebar-icon").click(function() {                
							  if (toggle)
							  {
								$(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
								$("#menu span").css({"position":"absolute"});
							  }
							  else
							  {
								$(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
								setTimeout(function() {
								  $("#menu span").css({"position":"relative"});
								}, 400);
							  }
											
											toggle = !toggle;
										});
							</script>
<!--js -->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.min.js"></script>
</body>
</html>