<?php
include('dbconn.php');
error_reporting(0);
@ini_set('display_errors', 0);
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  require 'php/src/Exception.php';
  require 'php/src/PHPMailer.php';
  require 'php/src/SMTP.php';

  require 'vendor/autoload.php';
  $mail = new PHPMailer(true);

  $output = '';

  if (isset($_POST['submit'])) {
   
    $name = $_POST['uname'];
    $sql = "SELECT * FROM users WHERE uname = '{$name}'";
    $res = mysqli_query($conn,$sql);
    $rows = mysqli_fetch_assoc($res);
    $email = $rows['uemail'];
    $email2 = 'infobookstore3@gmail.com';
    $id = $rows['ID'];

    try {
      $mail->isSMTP();
      $mail->Host = 'smtp.gmail.com';
      $mail->SMTPAuth = true;
      $mail->Username = 'infobookstore3@gmail.com';
      $mail->Password = 'Abhi@6601';
      $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
      $mail->Port = 587;
      $mail->setFrom($email,'Book Store');
      $mail->addAddress($email);
      $mail->isHTML(true);
      $num = rand(999999999,10000000000);
      $mail->Subject = 'Password Reset Link';
      $mail->Body = '<h2>Click The Below Button To Reset Your Password <br></h2> 
      <a href = "http://localhost/bookstore/resetpass.php?name='.$name.'&&id='.$id.'&&tokenId='.$num.'"> <button style = "height:35px;
      widht:400px;
      background-color:dodgerblue;
      color:white;
      border:none;
      border-radius:5px;">Click Here</button> </a>';

      $mail->send();
      $output = '<div class="alert alert-success">
                  <h5>Your Password has been sent to Registered Email Address!</h5>
                </div>';
    } catch (Exception $e) {
      $output = '<div class="alert alert-danger">
                  <h5> User Not Registered </h5>
                </div>';
    }
  }


?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <link rel="stylesheet" href="css/style.css">

    <title>Forget Password</title>
  </head>
  <body>
  

       
  <div class="content">
    <div class="container">
    
      <div class="row">

        <div class="col-md-6 order-md-2">
          <img src="images/fg.jpg" alt="Image" class="img-fluid">
        </div>

        <div class="col-md-6 contents">
          <div class="form-group">
               <center> <?php echo $output; ?></center>
              </div>
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="mb-4">
              <h3>Forgot Password? Chill we got this!</h3>
              <p class="mb-4"></p>
            </div>
            <form action="" method="post">
              <div class="form-group first">
                <label for="email"></label>
                <input type="text" class="form-control" id="username" name = "uname" placeholder="Enter Registered Username">

              </div>

              <input type="submit" value="Submit" name = "submit" class="btn text-white btn-block btn-primary">

              <span class="d-block text-left my-4 text-muted"><a href = "login.php">or login here</a></span>
            </form>
        
            </div>
          </div>
          
        </div>
        
      </div>
    </div>
  </div>

  
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>