<?php
session_start();
include('dbconn.php');
$name = $_SESSION['uname'];
if(!isset($name)) {
	header("location:index.html");
}
$name = $_SESSION['uname'];
$newquery = "SELECT * FROM users WHERE uname = '$name'";
$result = mysqli_query($conn,$newquery);
$rrows = mysqli_fetch_assoc($result);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Book Store</title>
    <script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
     <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <meta name="theme-color" content="#03a6f3">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
   
<body>
     <script>
            AOS.init ({
                offset:200,
                duration:1000
            });
        </script>
    <header>
        <div class="main-menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="navbar-item active">
                                <a href="index.php" class="nav-link">Home</a>
                            </li>
                            <li class="navbar-item">
                                <a href="books.php" class="nav-link">Read</a>
                            </li>
                            <li class="navbar-item">
                                <a href="about.php" class="nav-link">About</a>
                            </li>
                            <li class="navbar-item">
                                <a href="profile.php" class="nav-link"><?php echo $_SESSION['uname'];?></a>
                            </li>
                            <li class="navbar-item">
                                <a href="logout.php" class="nav-link">Logout</a>
                            </li>
                            
                        </ul>
                        <?php
                        $uid = $rrows['ID'];
                          $sql = "SELECT * FROM bookmark WHERE uid = '$uid'";
                          $result = mysqli_query($conn,$sql);
                          $rows = mysqli_num_rows($result);
                        ?>
                        <a href = "ubm.php">
                            <div class="cart my-2 my-lg-0">
                            <span>
                                <i style="color: #89CFF0;" class="fa fa-bookmark-o" aria-hidden="true"></i></span>
                                 <span class="quntity"><?php echo $rows; ?></span>
                        </div></a>
                        <form class="form-inline my-2 my-lg-0" action="search" method="POST">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search here..." aria-label="Search" name = "search" required>
                            <input type="submit" value = "🔍" style  = "background-color: white;">
                        </form>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <section class="slider">
        <div class="container">
            <div id="owl-demo" class="owl-carousel owl-theme">
                <div class="item">
                    <div class="slide">
                        <img src="images/slide1.jpg" alt="slide1">
                        <div class="content">
                            <div class="title">
                                <h3>welcome to bookstore</h3>
                                <h5>Discover the best books online with us</h5>
                                <a href="books.php" class="btn">read books</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="slide">
                        <img src="images/slide2.jpg" alt="slide1">
                        <div class="content">
                            <div class="title">
                                <h3>welcome to bookstore</h3>
                                <h5>A room without books is like a body without a soul.</h5>
                                <a href="books.php" class="btn">read books</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="slide">
                        <img src="images/slide3.jpg" alt="slide1">
                        <div class="content">
                            <div class="title">
                                <h3>welcome to bookstore</h3>
                                <h5>Take a good book to bed with you—books do not snore.</h5>
                                <a href="books.php" class="btn">read books</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="slide">
                        <img src="images/slide4.jpg" alt="slide1">
                        <div class="content">
                            <div class="title">
                                <h3>welcome to bookstore</h3>
                                <h5>Books are a uniquely portable magic.</h5>
                                <a href="books.php" class="btn">read books</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="recomended-sec">
        <div class="container">
            <div class="title">
                <h2>Categorized Books</h2>
                <hr>
            </div>
            <div class="row" data-aos = "fade-in">
                <div class="col-lg-3 col-md-6">
                    <div class="item">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIJFuAStKlndnSwnEHVPKmy3v_Wh3kkoWfew&usqp=CAU" alt="img">
                        <h3>Sci - Fi Books</h3>
                        <h6><a href="#">Read now</a></h6>
                        <div class="hover">
                            <a href="category?cat=sci-fi">
                            <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="item">
                        <img src="https://wallpapercave.com/wp/wp3539323.jpg" alt="img">
                        <h3>Fantasy Books</h3>
                        <h6> <a href="#">Read now</a></h6>
                        
                        <div class="hover">
                            <a href="category?cat=fantasy">
                            <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="item">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPDz7PU2GlwrDD52tlEctcT7591WlS1172xQ&usqp=CAU" alt="img">
                        <h3>Comedy Books</h3>
                        <h6> <a href="#">Read now</a></h6>
                        <div class="hover">
                            <a href="category?cat=comedy">
                            <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="item">
                        <img src="https://i.pinimg.com/originals/f1/bc/eb/f1bceb5ee10438f2eb3c4d656894b27c.jpg" alt="img">
                        <h3>Horror Books</h3>
                        <h6> <a href="#">Read now</a></h6>
                        <div class="hover">
                            <a href="category?cat=horror">
                            <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-sec">
        <div class="about-img">
            <figure style="background:url(./images/about-img.jpg)no-repeat;"></figure>
        </div>
        <div class="about-content">
            <h2>About bookstore,</h2>
            <p>The main objective of the project is to create an online book store that allows users to search and read the book. Using this Website the user can read online instead of going out to a book store and wasting time. </p>
            <p>The information is complete, more convenient retrieval , the new book information on the new, consumers can see in a timely manner</p>
            <div class="btn-sec">
                <a href="books.php" class="btn yellow">Read books</a>
                
            </div>
        </div>
    </section>
    <section class="recent-book-sec">
        <div class="container">
            <div class="title">
                <h2>highly recommendes books</h2>
                <hr>
            </div>
            <div class="row" >
                                <?php 
                include('dbconn.php');
                $sql = "SELECT * FROM books order by rand() limit 10";
                $res = mysqli_query($conn,$sql);
                while($row = mysqli_fetch_assoc($res)) {
                    ?>
                <div class="col-lg-2 col-md-3 col-sm-4" data-aos = "fade-in">
                    <div class="item">
                       <a href="product-single?id=<?php echo $row['ID']?>&&btype=<?php echo $row['btype']?>"><?php echo "<img src ='data:image/jpeg;base64,".$row["bimg1"]." ' ";?></a>
                        <h3><a href="product-single?id=<?php echo $row['ID']?>&&btype=<?php echo $row['btype']?>"><?php echo $row['bname']; ?></a></h3>
                        <h6><a href="product-single?id=<?php echo $row['ID']?>&&btype=<?php echo $row['btype']?>">Read Now</a></h6>
                    </div>
                </div>
                <?php 
            } 
            ?>
              </div>
            <div class="btn-sec">
                <a href="books.php" class="btn gray-btn">view all books</a>
            </div>
        </div>
    </section>
    <section class="features-sec">
        <div class="container">
            <ul>
               <li>
                    <span class="icon"><i class="fa fa-book" aria-hidden="true"></i></span>
                    <h3>ENJOYABLE READING</h3>
                    <h5>Safe Reading Guarantee</h5>
                    
                </li>
                <li>
                    <span class="icon return"><i class="fa fa-money" aria-hidden="true"></i></span>
                    <h3>NO MONEY REQUIERD</h3>
                    <h5>BookStore is free for all</h5>
                    
                </li>
                <li>
                    <span class="icon chat"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span>
                    <h3>PDF</h3>
                    <h5>Digital PDF</h5>
                   
                </li>
            </ul>
        </div>
    </section>
    <section class="offers-sec" style="background:url(images/offers.jpg)no-repeat;">
        <div class="cover"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="detail">
                        <h3>New Arrivals</h3>
                        <h6>Checkout our newly added books. we hope you like it</h6>
                        <a href="na" class="btn blue-btn">view all books</a>
                        <span class="icon-point percentage">
                            <img src="images/new.png" alt="">
                        </span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="detail">
                        <h3>Bookmark</h3>
                        <h6>You can bookmark other books you can read later.</h6>
                        <a href="ubm" class="btn blue-btn">view your selected books</a>
                        <span class="icon-point amount"><img src="images/amount.png" alt=""></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial-sec">
        <div class="container">
            <div id="testimonal" class="owl-carousel owl-theme">
                <div class="item">
                    <h3>Chetan Bhagat is an Indian author and columnist. He was included in Time magazine's list of World's 100 Most Influential People in 2010. Bhagat graduated in mechanical engineering at IIT Delhi and completed an MBA at IIM Ahmedabad..</h3>
                    <div class="box-user">
                        <h4 class="author">Chetan Bhagat</h4>
                        <span class="country">India</span>
                    </div>
                </div>
                <div class="item">
                    <h3>Stephen Edwin King (born September 21, 1947) is an American author of horror, supernatural fiction, suspense, crime, science-fiction, and fantasy novels. His books have sold more than 350 million copies, and many have been adapted into films, television series, miniseries, and comic books.</h3>
                    <div class="box-user">
                        <h4 class="author">Stephen King</h4>
                        <span class="country">UK</span>
                    </div>
                </div>
                <div class="item">
                    <h3>William Shakespeare was an English playwright, poet, and actor, widely regarded as the greatest writer in the English language and the world's greatest dramatist. He is often called England's national poet and the "Bard of Avon".</h3>
                    <div class="box-user">
                        <h4 class="author">William Shakespeare</h4>
                        <span class="country">USA</span>
                    </div>
                </div>
                <div class="item">
                    <h3>Dante Alighieri, probably baptized Durante di Alighiero degli Alighieri and often referred to simply as Dante, was an Italian poet, writer and philosopher.</h3>
                    <div class="box-user">
                        <h4 class="author">Dante Alighieri</h4>
                        <span class="country">Italy</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="left-quote">
            <img src="images/left-quote.png" alt="quote">
        </div>
        <div class="right-quote">
            <img src="images/right-quote.png" alt="quote">
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="address">
                        <h4>Our Address</h4>
                        <h6>flat no.204,Aravalli building,Dipti sky city</h6>
                        <h6>Call : 9511962448</h6>
                        <h6>Email : infobookstore3@gmail.com</h6>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="navigation">
                        <h4>Navigation</h4>
                        <ul>
                            <li><a href="index">Home</a></li>
                            <li><a href="about">About Us</a></li>
                            <li><a href="privacy-policy">Privacy Policy</a></li>
                            <li><a href="terms-conditions">Terms</a></li>
                        </ul>
                    </div>
                    <div class="navigation">
                        <h4>Help</h4>
                        <ul>
                            <li><a href="privacy-policy">Privacy</a></li>
                            <li><a href="faq">FAQ’s</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form">
                        <h3>Quick Contact us</h3>
                        <h6>If you have any problem, you can leave a message</h6>
                            <div class="row">
                                <div class="col-md-6">
                                    <input placeholder="Name"  id = "name">
                                </div>
                                <div class="col-md-6">
                                    <input type="email" placeholder="Email"  id = "email" required>
                                </div>
                                <div class="col-md-12">
                                    <textarea placeholder="Messege" id = "message"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn black" id="submit">Alright, Submit</button>
                                </div>
                                <script type="text/javascript">
                                    $("#submit").on("click",function() {
                                      var name = $("#name").val();
                                      var email = $("#email").val();
                                      var message = $("#message").val();
                                      if(name == "" || email == "" || message == "") {
                                        swal("Oops", "For Cantacting i guess you have to fill the whole form", "error"); 
                                      }
                                      else {
                                        $.ajax({
                                             url:"insertmsg.php",
                                             type:"POST",
                                             data:{name:name,email:email,message:message},
                                             success:function(data) {
                                                if(data == 1) {
                                                     swal("Message Sent", "Thanks For contacting our support please check your email for a reply soon", "success"); 
                                                }
                                                else if(data == 3){
                                                    swal("Invalid Email", "The Email Address You Entered is not correct", "error");
                                                }
                                                else {
                                                    alert(data);
                                                }
                                             }
                                        });
                                      }
                                    });
                                </script>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h5>(C) 2021. All Rights Reserved. BookStore</h5>
                    </div>
                    <div class="col-md-6">
                        <div class="share align-middle">
                            <a href="https://www.facebook.com/abhishek.nagari.3/"><span class="fb"><i class="fa fa-facebook-official"></i></span></a>
                           <a href="https://www.instagram.com/mr._.awesome._/"><span class="instagram"><i class="fa fa-instagram"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>