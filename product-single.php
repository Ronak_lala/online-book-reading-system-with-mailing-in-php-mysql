<?php
session_start();
$name = $_SESSION['uname'];
if(!isset($name)) {
    header("location:login");
}
$id = $_GET['id'];
$btype = $_GET['btype'];
include('dbconn.php');
$name = $_SESSION['uname'];
$newquery = "SELECT * FROM users WHERE uname = '$name'";
$result = mysqli_query($conn,$newquery);
$rrows = mysqli_fetch_assoc($result);

$sql = "SELECT * FROM books WHERE btype = '{$btype}' AND ID = '{$id}'";
$res = mysqli_query($conn,$sql);
$rows = mysqli_fetch_assoc($res);



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Store</title>
    <script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#03a6f3">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body>
    <header>
        <div class="main-menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                         <ul class="navbar-nav ml-auto">
                            <li class="navbar-item active">
                                <a href="index" class="nav-link">Home</a>
                            </li>
                            <li class="navbar-item">
                                <a href="books.php" class="nav-link">Read</a>
                            </li>
                            <li class="navbar-item">
                                <a href="about.php" class="nav-link">About</a>
                            </li>
                            <li class="navbar-item">
                                <a href="profile.php" class="nav-link"><?php echo $_SESSION['uname'];?></a>
                            </li>
                            <li class="navbar-item">
                                <a href="logout.php" class="nav-link">Logout</a>
                            </li>
                            
                        </ul>
                        <?php
                        $uid = $rrows['ID'];
                          $nsql = "SELECT * FROM bookmark WHERE uid = '$uid'";
                          $nresult = mysqli_query($conn,$nsql);
                          $nrows = mysqli_num_rows($nresult);
                        ?>
                        <a href = "ubm.php"><div class="cart my-2 my-lg-0">
                            <span>
                                <i style="color: #89CFF0;" class="fa fa-bookmark-o" aria-hidden="true"></i></span>
                            <span class="quntity"><?php echo $nrows; ?></span>
                        </div></a>
                           <form class="form-inline my-2 my-lg-0" action="search"  method="POST">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search here..." aria-label="Search" name = "search" required>
                            <input type="submit" value = "🔍" style="background-color: white;">
                        </form>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <div class="breadcrumb">
        <div class="container">
            <a class="breadcrumb-item" href="index.html">Home</a>
            <span class="breadcrumb-item active">Product</span>
        </div>
    </div>
    <section class="product-sec">
        <div class="container">
            <h1><?php echo $rows['bname']; ?></h1>
            <div class="row">



                                <?php echo "<img width = '300' src ='data:image/jpeg;base64,".$rows["bimg1"]." ' ";?>

                            
   
                        <!-- main slider carousel nav controls -->
                      
                    <!--/main slider carousel-->

                <div class="col-md-6 slider-content">
                    <p><?php echo $rows['bdesc'] ?></p>
                    <p><h3><?php echo $rows['bviews'] ?> Views on this book</h3></p>
                    <div class="btn-sec">
                        <a href = "read.php?bid=<?php echo $rows['ID'];?>"><button class="btn">Read Now</button></a>
                        <button class="btn black" id="bookmark">BookMark It</button>
                        <input type = "hidden" value="<?php echo $rows['ID']; ?>" id = "h1">
                        <input type = "hidden" value="<?php echo $rows['btype']; ?>" id = "h2">
                        <input type = "hidden" value="<?php echo $rrows['ID']; ?>" id = "h3">
                        <script type="text/javascript">
                            $("#bookmark").on("click",function() {
                                var id = $("#h1").val();
                                var btype = $("#h2").val();
                                var uid = $("#h3").val();
                                $.ajax({
                                    url: "bookmark.php",
                                    type: "POST",
                                    data:{id:id,btype:btype,uid:uid},
                                    success:function(data) {
                                          if(data == 1) {
                                             swal("BookMark Added", "Bookmark successfully Added", "success");
                                          }
                                          else if(data == 2){
                                            swal("Already Added", "The book you trying to add is already added", "warning");
                                          }
                                          else {
                                            swal("Oops", data, "error");
                                          }
                                    }
                                });
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="related-books">
        <div class="container">
            <h2>You may also like these book</h2>
            <div class="recomended-sec">
                <div class="row">
                    <?php 
                    $sql = "SELECT * FROM books WHERE btype = '{$btype}' order by rand() limit 4";
                    $res = mysqli_query($conn,$sql);
                    while($row = mysqli_fetch_assoc($res)) {
                        ?>
                       <a href = 'product-single?id=<?php echo $row['ID']?>&&btype=<?php echo $row['btype']?>'> <div class="col-lg-3 col-md-6">
                        <div class="item">
                            <?php echo "<img height = '300' src ='data:image/jpeg;base64,".$row["bimg1"]." ' ";?><br>
                            <h3 style="font-weight: 400;color:black;"><?php echo $row['bname']; ?></h3>
                            <h6> <a href="product-single?id=<?php echo $row['ID']?>&&btype=<?php echo $row['btype']?>">Read Now</a></h6>
                            
                        </div>
                    </div></a>
                        <?php
                    }
                    ?>
                    
                    
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="address">
                        <h4>Our Address</h4>
                        <h6>flat no.204,Aravalli building,Dipti sky city</h6>
                        <h6>Call : 9511962448</h6>
                        <h6>Email : infobookstore3@gmail.com</h6>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="navigation">
                        <h4>Navigation</h4>
                        <ul>
                            <li><a href="index">Home</a></li>
                            <li><a href="about">About Us</a></li>
                            <li><a href="privacy-policy">Privacy Policy</a></li>
                            <li><a href="terms-conditions">Terms</a></li>
                        </ul>
                    </div>
                    <div class="navigation">
                        <h4>Help</h4>
                        <ul>
                            <li><a href="privacy-policy">Privacy</a></li>
                            <li><a href="faq">FAQ’s</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form">
                        <h3>Quick Contact us</h3>
                        <h6>If you have any problem, you can leave a message</h6>
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <input placeholder="Name" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="email" placeholder="Email" required>
                                </div>
                                <div class="col-md-12">
                                    <textarea placeholder="Messege"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn black">Alright, Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h5>(C) 2021. All Rights Reserved. BookStore</h5>
                    </div>
                    <div class="col-md-6">
                       <div class="share align-middle">
                            <a href="https://www.facebook.com/abhishek.nagari.3/"><span class="fb"><i class="fa fa-facebook-official"></i></span></a>
                           <a href="https://www.instagram.com/mr._.awesome._/"><span class="instagram"><i class="fa fa-instagram"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>