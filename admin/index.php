<!doctype html>
<html lang="en">
<?php
if (!isset ($_GET['msg']) ) {  
    $page = '';  
} else {  
    $page = "Incorrect Username/Password";
}  
?>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Style -->
    <link rel="stylesheet" href="css/style.css">

    <title>Login Admin</title>
  </head>
  <body>
  

  
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-6 order-md-2">
          <img src="images/undraw_file_sync_ot38.svg" alt="Image" class="img-fluid">
        </div>
        <div class="col-md-6 contents">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="mb-4">
              <h3>Log In to <strong>Admin</strong></h3>
              <p class="mb-4"><!-- Sample text -->.</p>
            </div>
            <form action="alog.php" method="post">
              <div class="form-group first">
                <label for="username"></label>
                <input name="aname" type="text" class="form-control" id="username" placeholder="Username">

              </div>
              <div class="form-group last mb-4">
                <label for="password"></label>
                <input name="apass" type="password" class="form-control" id="password" placeholder="Password">
                
              </div>
              
                
                <span class="ml-auto"><a href="frgpass" class="forgot-pass">Forgot Password</a></span> 
<br><br>

              <input type="submit" value="Log In" class="btn text-white btn-block btn-primary">
   <div class="incorrect" style="color: red;">
    <?php
     echo "$page";
   ?>
    
   </div>
   
              <span class="d-block text-left my-4 text-muted"><a href = "signup">or Signup here</a></span>
              
          
            </form>
            </div>
          </div>
          
        </div>
        
      </div>
    </div>
  </div>

  
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>