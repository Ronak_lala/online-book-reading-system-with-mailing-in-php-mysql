<?php
session_start();
include('dbconn.php');
$name = $_SESSION['uname'];
if(!isset($name)) {
	header("location:index.html");
}
$name = $_SESSION['uname'];
$newquery = "SELECT * FROM users WHERE uname = '$name'";
$result = mysqli_query($conn,$newquery);
$rrows = mysqli_fetch_assoc($result);
$uid = $rrows['ID'];

$asql = "SELECT * FROM readbooks WHERE uid = '{$uid}'";
$nresult = mysqli_query($conn,$asql);
$count = mysqli_num_rows($nresult);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Book Store</title>
    <script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <meta name="theme-color" content="#03a6f3">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/styles.css">
</head>

<body>
    <header>
        <div class="main-menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="navbar-item active">
                                <a href="index.php" class="nav-link">Home</a>
                            </li>
                            <li class="navbar-item">
                                <a href="books.php" class="nav-link">Read</a>
                            </li>
                            <li class="navbar-item">
                                <a href="about.php" class="nav-link">About</a>
                            </li>
                            <li class="navbar-item">
                                <a href="profile.php" class="nav-link"><?php echo $_SESSION['uname'];?></a>
                            </li>
                            <li class="navbar-item">
                                <a href="logout.php" class="nav-link">Logout</a>
                            </li>
                            
                        </ul>
                        <?php
                        $uid = $rrows['ID'];
                          $sql = "SELECT * FROM bookmark WHERE uid = '$uid'";
                          $result = mysqli_query($conn,$sql);
                          $rows = mysqli_num_rows($result);
                        ?>
                        <a href = "ubm.php">
                            <div class="cart my-2 my-lg-0">
                            <span>
                                <i style="color: #89CFF0;" class="fa fa-bookmark-o" aria-hidden="true"></i></span>
                                 <span class="quntity"><?php echo $rows; ?></span>
                        </div></a>
                        <form class="form-inline my-2 my-lg-0" action="search"  method="POST">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search here..." aria-label="Search" name = "search" required>
                            <input type="submit" value = "🔍" style  = "background-color: white;">
                        </form>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    
    
    <section class="recent-book-sec">
        <div class="container">
            <style type="text/css">
        .section {
            height: 300px;
            width: 100%;
        }
        .section .left {
            height: 100%;
            width: 20%;
            float:left;
            border-right: 1px solid black;
        }
        .section .left img {
            height: 40%;
            width: 60%;
            margin-left:10%;
            margin-top:30%;
        }
        .section .right {
            float: right;
            height: 100%;
            width: 79%;
            text-align: left;
           
        }
        .section .right h3, .section .right h4, .section .right h5 {
            line-height: 45px;
            font-weight: 300;
        }
        .section .right label {
            font-weight: 500;
        }
    </style>
    <div class="section">
        <div class="left">
             <img src="https://img.icons8.com/ios/100/000000/user--v1.png"/>
        </div>
        <div class="right">
            <h4><labeL>Name :</labeL> &nbsp;&nbsp;<?php echo $rrows['fname']; ?></h4>
            <h4><labeL>User Name : </labeL> &nbsp;&nbsp;<?php echo $rrows['uname']; ?></h4>
            <h4><labeL>Email Address : </labeL> &nbsp;&nbsp;<?php echo $rrows['uemail']; ?></h4>
            <h4><labeL>Phone Number : </labeL> &nbsp;&nbsp;<?php echo $rrows['phno']; ?></h4>
            
            <h4><labeL>Number of books Read : </labeL> &nbsp;&nbsp; <?php echo $count; ?></h4>
            <h4><labeL>Number of books Bookmarked : </labeL> &nbsp;&nbsp;<?php echo $rows; ?></h4>
       </div>
    </div><br><br>
            <div class="title">
                <h2>Recently read books</h2>
                <hr>
            </div>
            <div class="row">
                                <?php 
                include('dbconn.php');
                $sql = "SELECT * FROM readbooks WHERE uid = '{$uid}' AND recent = 1";
                $res = mysqli_query($conn,$sql);
                while($row = mysqli_fetch_assoc($res)) {
                    
                    $bid = $row['bid'];
                    $csql = "SELECT * FROM books WHERE ID = '{$bid}'";
                    $cres = mysqli_query($conn,$csql);
                    while($rowsas = mysqli_fetch_assoc($cres)) {

                    ?>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="item">
                       <a href="read.php?bid=<?php echo $rowsas['ID']?>"><?php echo "<img src ='data:image/jpeg;base64,".$rowsas["bimg1"]." ' ";?></a> 
                        <h3><a href="read.php?bid=<?php echo $rowsas['ID']?>"><?php echo $rowsas['bname']; ?></a></h3>
                        <h6><a href="read.php?bid=<?php echo $rowsas['ID']?>">Continue Reading</a></h6>
                    </div>
                </div>
                <?php 
            
            }
            } 
            ?>

 <?php 
                include('dbconn.php');
                $sql = "SELECT * FROM readbooks WHERE uid = '{$uid}' AND recent = 0 order by ID DESC";
                $res = mysqli_query($conn,$sql);
                while($row = mysqli_fetch_assoc($res)) {
                    
                    $bid = $row['bid'];
                    $csql = "SELECT * FROM books WHERE ID = '{$bid}'";
                    $cres = mysqli_query($conn,$csql);
                    while($rowsas = mysqli_fetch_assoc($cres)) {

                    ?>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="item">
                       <a href="read.php?bid=<?php echo $rowsas['ID']?>"><?php echo "<img src ='data:image/jpeg;base64,".$rowsas["bimg1"]." ' ";?></a> 
                        <h3><a href="read.php?bid=<?php echo $rowsas['ID']?>"><?php echo $rowsas['bname']; ?></a></h3>
                        <h6><a href="read.php?bid=<?php echo $rowsas['ID']?>">Continue Reading</a></h6>
                    </div>
                </div>
                <?php 
            
            }
            } 
            ?>
           
              </div>
        </div>
    </section>
    
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="address">
                        <h4>Our Address</h4>
                        <h6>flat no.204,Aravalli building,Dipti sky city</h6>
                        <h6>Call : 9511962448</h6>
                        <h6>Email : infobookstore3@gmail.com</h6>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="navigation">
                        <h4>Navigation</h4>
                        <ul>
                            <li><a href="index">Home</a></li>
                            <li><a href="about">About Us</a></li>
                            <li><a href="privacy-policy">Privacy Policy</a></li>
                            <li><a href="terms-conditions">Terms</a></li>
                            <li><a href="products">Products</a></li>
                        </ul>
                    </div>
                    <div class="navigation">
                        <h4>Help</h4>
                        <ul>
                            <li><a href="privacy-policy">Privacy</a></li>
                            <li><a href="faq">FAQ’s</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form">
                        <h3>Quick Contact us</h3>
                        <h6>If you have any problem, you can leave a message</h6>
                            <div class="row">
                                <div class="col-md-6">
                                    <input placeholder="Name"  id = "name">
                                </div>
                                <div class="col-md-6">
                                    <input type="email" placeholder="Email"  id = "email" required>
                                </div>
                                <div class="col-md-12">
                                    <textarea placeholder="Messege" id = "message"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn black" id="submit">Alright, Submit</button>
                                </div>
                                <script type="text/javascript">
                                    $("#submit").on("click",function() {
                                      var name = $("#name").val();
                                      var email = $("#email").val();
                                      var message = $("#message").val();
                                      if(name == "" || email == "" || message == "") {
                                        swal("Oops", "For Cantacting i guess you have to fill the whole form", "error"); 
                                      }
                                      else {
                                        $.ajax({
                                             url:"insertmsg.php",
                                             type:"POST",
                                             data:{name:name,email:email,message:message},
                                             success:function(data) {
                                                if(data == 1) {
                                                     swal("Message Sent", "Thanks For contacting our support please check your email for a reply soon", "success"); 
                                                }
                                                else if(data == 3){
                                                    swal("Invalid Email", "The Email Address You Entered is not correct", "error");
                                                }
                                                else {
                                                    alert(data);
                                                }
                                             }
                                        });
                                      }
                                    });
                                </script>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h5>(C) 2021. All Rights Reserved. BookStore</h5>
                    </div>
                    <div class="col-md-6">
                                              <div class="share align-middle">
                            <a href="https://www.facebook.com/abhishek.nagari.3/"><span class="fb"><i class="fa fa-facebook-official"></i></span></a>
                           <a href="https://www.instagram.com/mr._.awesome._/"><span class="instagram"><i class="fa fa-instagram"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>