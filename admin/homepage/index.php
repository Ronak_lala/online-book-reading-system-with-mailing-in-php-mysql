

<?php 
include('../dbconn.php');
session_start();
$name = $_SESSION['username'];
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Bookstore : Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Augment Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/amcharts.js"></script>	
<script src="js/serial.js"></script>	
<script src="js/light.js"></script>	
<script src="js/radar.js"></script>	
<link href="css/fabochart.css" rel='stylesheet' type='text/css' />
<!--clock init-->
<script src="js/css3clock.js"></script>
<!--Easy Pie Chart-->
<!--skycons-icons-->
<script src="js/skycons.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <meta name="theme-color" content="#03a6f3">


<!--//skycons-icons-->
</head> 
<body>
   <div class="page-container">
   <!--/content-inner-->
	<div class="left-content">
	   <div class="inner-content">
		<!-- header-starts -->
			<div class="header-section">
						<!--menu-right-->
						<div class="top_menu">
						       
									
							<!--/profile_details-->
								<div class="profile_details_left">
									<ul class="nofitications-dropdown">
											<li class="dropdown note dra-down">
													
																		<script type="text/javascript">
			
																	function DropDown(el) {
																		this.dd = el;
																		this.placeholder = this.dd.children('span');
																		this.opts = this.dd.find('ul.dropdown > li');
																		this.val = '';
																		this.index = -1;
																		this.initEvents();
																	}
																	DropDown.prototype = {
																		initEvents : function() {
																			var obj = this;

																			obj.dd.on('click', function(event){
																				$(this).toggleClass('active');
																				return false;
																			});

																			obj.opts.on('click',function(){
																				var opt = $(this);
																				obj.val = opt.text();
																				obj.index = opt.index();
																				obj.placeholder.text(obj.val);
																			});
																		},
																		getValue : function() {
																			return this.val;
																		},
																		getIndex : function() {
																			return this.index;
																		}
																	}

																	$(function() {

																		var dd = new DropDown( $('#dd') );

																		$(document).click(function() {
																			// all dropdowns
																			$('.wrapper-dropdown-3').removeClass('active');
																		});

																	});

																</script>
										    </li>
									      <?php
												$sql = "SELECT * FROM contact order by ID DESC limit 3";
                                                           $res = mysqli_query($conn,$sql);
                                                           $count = mysqli_num_rows($res);

												?>
										<li class="dropdown note">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope-o"></i> <span class="badge"><?php echo $count; ?></span></a>
												<?php
function time_ago_in_php($timestamp){
  
  date_default_timezone_set("Asia/Kolkata");         
  $time_ago        = strtotime($timestamp);
  $current_time    = time();
  $time_difference = $current_time - $time_ago;
  $seconds         = $time_difference;
  
  $minutes = round($seconds / 60); 
  $hours   = round($seconds / 3600);
  $days    = round($seconds / 86400);   
  $weeks   = round($seconds / 604800);  
  $months  = round($seconds / 2629440);  
  $years   = round($seconds / 31553280); 
                
  if ($seconds <= 60){

    return "Just Now";

  } else if ($minutes <= 60){

    if ($minutes == 1){

      return "one minute ago";

    } else {

      return "$minutes minutes ago";

    }

  } else if ($hours <= 24){

    if ($hours == 1){

      return "an hour ago";

    } else {

      return "$hours hrs ago";

    }

  } else if ($days <= 7){

    if ($days == 1){

      return "yesterday";

    } else {

      return "$days days ago";

    }

  } else if ($weeks <= 4.3){

    if ($weeks == 1){

      return "a week ago";

    } else {

      return "$weeks weeks ago";

    }

  } else if ($months <= 12){

    if ($months == 1){

      return "a month ago";

    } else {

      return "$months months ago";

    }

  } else {
    
    if ($years == 1){

      return "one year ago";

    } else {

      return "$years years ago";

    }
  }
}
												?>
												<?php
												$sql = "SELECT * FROM contact order by ID DESC limit 3";
                                                           $res = mysqli_query($conn,$sql);
                                                           $count = mysqli_num_rows($res);

												?>
													<ul class="dropdown-menu two first">
														<li>
															<div class="notification_header">
																<h3>You have <?php echo $count;?> new messages  </h3> 
															</div>
														</li>
														<?php 
                                                           
                                                           while($row = mysqli_fetch_assoc($res)) {
                                                           	$time = $row['dates'];
                                                           	?>
                                                           	<li><a href="#">
														   <div class="user_img"><img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png" alt=""></div>
														   <div class="notification_desc">
															<p><?php echo $row['name']; ?> Contacted You</p>
															<p><span><?php echo time_ago_in_php($time); ?></span></p>
															</div>
														   <div class="clearfix"></div>	
														 </a></li>
                                                           	<?php
                                                           }
														?>
														
														
														 
															<div class="notification_bottom">
																<a href="inbox.php">See all messages</a>
															</div> 
														</li>
													</ul>
										</li>
										<?php
$asql = "SELECT * FROM users order by ID DESC limit 5";
$ares = mysqli_query($conn,$asql);
$count = mysqli_num_rows($ares);
?>
							<li class="dropdown note">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell-o"></i> <span class="badge"><?php echo $count; ?></span></a>

									<ul class="dropdown-menu two">
										<li>
											<div class="notification_header">
												<h3>You have <?php echo $count; ?> new user on BookStore</h3>
											</div>
										</li>
										<?php
										while($arow = mysqli_fetch_assoc($ares)) {
?>
<li><a href="#">
											<div class="user_img"><img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png" alt=""></div>
										   <div class="notification_desc">
											<p><?php echo $arow['uname']; ?> Has Registered</p>
											</div>
										  <div class="clearfix"></div>	
										 </a></li>
<?php
}
										?>
										
										
									</ul>
							</li>	
							<li class="dropdown note">
								<a href="../../login.php" class="dropdown-toggle"  aria-expanded="false"><i class="fa fa-home"></i> </a>
							</li>
									 							   		
							<div class="clearfix"></div>	
								</ul>
							</div>
							<div class="clearfix"></div>	
							<!--//profile_details-->
						</div>
						<!--//menu-right-->
					<div class="clearfix"></div>
				</div>
				<?php
															include('../dbconn.php');
															$sql = "SELECT * FROM books WHERE btype = 'comedy'";
															$res = mysqli_query($conn,$sql);
															$count1 = mysqli_num_rows($res);
															?>
															<?php
															$sql = "SELECT * FROM books WHERE btype = 'fantasy'";
															$res = mysqli_query($conn,$sql);
															$count2 = mysqli_num_rows($res);
															?>
															<?php
															$sql = "SELECT * FROM books WHERE btype = 'sci-fi'";
															$res = mysqli_query($conn,$sql);
															$count3 = mysqli_num_rows($res);
															?>
															<?php
															$sql = "SELECT * FROM books WHERE btype = 'horror'";
															$res = mysqli_query($conn,$sql);
															$count4 = mysqli_num_rows($res);

															$total = $count1 + $count2 + $count3 + $count4;

															$sqlnew = "SELECT * FROM users";
															$result = mysqli_query($conn,$sqlnew);
															$rows = mysqli_num_rows($result);

															$sqlnew2 = "SELECT * FROM admin";
															$result2 = mysqli_query($conn,$sqlnew2);
															$rows2 = mysqli_num_rows($result2);
															?>
					<!-- //header-ends -->
						<div class="outter-wp">
								<!--custom-widgets-->
												<div class="custom-widgets">
												   <div class="row-one">
														<div class="col-md-3 widget">
															<div class="stats-left ">
																<h5>Total</h5>
																<h4> Books</h4>
															</div>
															<div class="stats-right">
																<label><?php echo $total; ?></label>
															</div>
															<div class="clearfix"> </div>	
														</div>
														<div class="col-md-3 widget states-mdl">
															<div class="stats-left">
																<h5>Total</h5>
																<h4>Users</h4>
															</div>
															<div class="stats-right">
																<label> <?php echo $rows ?></label>
															</div>
															<div class="clearfix"> </div>	
														</div>
														<div class="col-md-3 widget states-thrd">
															<div class="stats-left">
																<h5>Total</h5>
																<h4>Admin</h4>
															</div>
															<div class="stats-right">
																<label><?php echo $rows2 ?></label>
															</div>
															<div class="clearfix"> </div>	
														</div>
														<div class="col-md-3 widget states-last">
															<div class="stats-left">
																<h5>Total % of Books</h5>
																<h4>Read</h4>
															</div>
															<?php
$bsql = "SELECT * FROM books";
$bres = mysqli_query($conn,$bsql);
$bcount = mysqli_num_rows($bres);

$csql = "SELECT DISTINCT(bid) FROM readbooks";
$cres = mysqli_query($conn,$csql);
$ccount = mysqli_num_rows($cres);

$total = $ccount / $bcount * 100;
$totalnew= round($total);
											?>
															<div class="stats-right">
																<label><?php echo $totalnew; ?> </label>
															</div>
															<div class="clearfix"> </div>	
														</div>
														<div class="clearfix"> </div>	
													</div>
												</div>
												<!--//custom-widgets-->
												<!--/candile-->
												
													<!--/candile-->
													
												<!--/charts-->
												<div class="charts">
												  <div class="chrt-inner">
												    <div class="chrt-bars">
														<div class="col-md-6 chrt-two">
														 <h3 class="sub-tittle">Books Chart </h3>
															<div id="chart2"></div>
															<script src="js/fabochart.js"></script>
															
															<script>
															$(document).ready(function () {
																data = {
																  'Horror' : <?php echo $count4 ?>, 
																  'Comedy' : <?php echo $count1 ?>,
																  'Sci-Fi' : <?php echo $count3 ?>,
																  'Fantasy' : <?php echo $count2 ?>
																};

																$("#chart1").faBoChart({
																  time: 500,
																  animate: true,
																  instantAnimate: true,
																  straight: false,
																  data: data,
																  labelTextColor : "#002561",
																});
																$("#chart2").faBoChart({
																  time: 2500,
																  animate: true,
																  data: data,
																  straight: true,
																  labelTextColor : "#002561",
																});
															});
															</script>
														</div>
															
																<div class="clearfix"> </div>
															</div>										
												<!--/float-charts-->
												
														
																<div class="clearfix"> </div>
														
												<div class="area">
													<?php
$dsql = "SELECT * FROM books WHERE btype = 'horror'";
$dres = mysqli_query($conn,$dsql);
$dcount = mysqli_num_rows($dres);
$type = "horror";

$rsql = "SELECT DISTINCT(bid) FROM readbooks";
$rres = mysqli_query($conn,$rsql);
$totaln = 0;
while($rrows = mysqli_fetch_assoc($rres)) {
      $bid = $rrows['bid'];
      $fsql = "SELECT * FROM books WHERE ID = '$bid'";
      $fres = mysqli_query($conn,$fsql);
      $frows = mysqli_fetch_assoc($fres);
      if($frows['btype'] == $type) {
      $totaln = $totaln + 1;
  }
}

$htotal = $totaln / $dcount * 100;
											?>


<?php
$dsql = "SELECT * FROM books WHERE btype = 'comedy'";
$dres = mysqli_query($conn,$dsql);
$dcount = mysqli_num_rows($dres);
$type = "comedy";

$rsql = "SELECT DISTINCT(bid) FROM readbooks";
$rres = mysqli_query($conn,$rsql);
$totalc = 0;
while($rrows = mysqli_fetch_assoc($rres)) {
      $bid = $rrows['bid'];
      $fsql = "SELECT * FROM books WHERE ID = '$bid'";
      $fres = mysqli_query($conn,$fsql);
      $frows = mysqli_fetch_assoc($fres);
      if($frows['btype'] == $type) {
      $totalc = $totalc + 1;
  }
}

$ctotal = $totalc / $dcount * 100;
											?>

											<?php
$dsql = "SELECT * FROM books WHERE btype = 'fantasy'";
$dres = mysqli_query($conn,$dsql);
$dcount = mysqli_num_rows($dres);
$type = "fantasy";

$rsql = "SELECT DISTINCT(bid) FROM readbooks";
$rres = mysqli_query($conn,$rsql);
$totalf = 0;
while($rrows = mysqli_fetch_assoc($rres)) {
      $bid = $rrows['bid'];
      $fsql = "SELECT * FROM books WHERE ID = '$bid'";
      $fres = mysqli_query($conn,$fsql);
      $frows = mysqli_fetch_assoc($fres);
      if($frows['btype'] == $type) {
      $totalf = $totalf+ 1;
  }
}

$ftotal = $totalf / $dcount * 100;
											?>
											
											<?php
$dsql = "SELECT * FROM books WHERE btype = 'Sci-fi'";
$dres = mysqli_query($conn,$dsql);
$dcount = mysqli_num_rows($dres);
$type = "Sci-fi";

$rsql = "SELECT DISTINCT(bid) FROM readbooks";
$rres = mysqli_query($conn,$rsql);
$totals = 0;
while($rrows = mysqli_fetch_assoc($rres)) {
      $bid = $rrows['bid'];
      $fsql = "SELECT * FROM books WHERE ID = '$bid'";
      $fres = mysqli_query($conn,$fsql);
      $frows = mysqli_fetch_assoc($fres);
      if($frows['btype'] == $type) {
      $totals = $totals + 1;
  }
}

$stotal = $totals / $dcount * 100;
											?>
												<script type = "text/javascript" src = "https://www.gstatic.com/charts/loader.js">
      </script>
      <script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart']});     
      </script>

      <div id = "container" style = "width: 550px; height: 400px; margin: 0 auto">
      </div>
      <script language = "JavaScript">
         function drawChart() {
            var data = google.visualization.arrayToDataTable([
               ['Year', 'Amount'],
               ['Horror',  <?php echo $totaln; ?>],
               ['Comedy',  <?php echo $totalc; ?>],
               ['Fantasy',   <?php echo $totalf; ?>],
               ['Sci-Fi',   <?php echo $totals; ?>]
            ]);

            var options = {title: 'Number of Books Read by Category'}; 

            var chart = new google.visualization.BarChart(document.getElementById('container'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

												</div>


													<!--//weather-charts-->
													<style type="text/css">
														.recent-book-sec {
  text-align: center;
  margin-bottom: 130px; }
  .recent-book-sec h2 {
    display: inline-block;
    padding: 0 15px;
    background: #fff;
    padding: 0 15px;
    position: relative;
    font-size: 30px;
    text-transform: uppercase;
    color: #363636; }
  .recent-book-sec hr {
    margin: 0;
    margin-top: -20px;
    border-top: 1px solid #999999; }
  .recent-book-sec .title {
    margin-bottom: 80px; }
  .recent-book-sec .item {
    text-align: left;
    margin-bottom: 45px; }
    .recent-book-sec .item img {
      margin-bottom: 15px;
      max-width: 100%;
      cursor: pointer; }
    .recent-book-sec .item img:hover + h3 {
      text-decoration: underline; }
    .recent-book-sec .item h3 {
      font-size: 16px;
      line-height: 22px;
      font-weight: 300;
      margin-bottom: 10px; }
      .recent-book-sec .item h3 a {
        color: inherit; }
    .recent-book-sec .item h6 {
      font-size: 12px; }
      .recent-book-sec .item h6 span {
        color: #ff0000; }
      .recent-book-sec .item h6 a {
        color: #000;
        border-bottom: 1px solid #d5d5d5;
        padding-bottom: 1px; }
        .recent-book-sec .item h6 a:hover {
          text-decoration: none;
          border-bottom: 0;
          color: #ff9700; }
  .recent-book-sec .row {
    margin: 0 -30px; }
    .recent-book-sec .row .col-lg-2 {
      padding: 0 30px;
      -ms-flex: 20%;
      flex: 20%;
      max-width: 20%; }
  .recent-book-sec .btn-sec {
    margin-top: 20px; }

													</style>
													 <section class="recent-book-sec">
        <div class="container">
													 <div class="row">
													 	<h1>Top 5 Read books on BookStore</h1><br>

                                <?php 
                include('../dbconn.php');
                $newsql = "select * from books group by ID having bviews = max(bviews) order by bviews DESC limit 5";
                $newres = mysqli_query($conn,$newsql);
                while ($newrowss = mysqli_fetch_assoc($newres)) {

                    ?>
                <div class="col-lg-2 col-md-3 col-sm-4" data-aos = "fade-in" style="margin-left:20px;">
                    <div class="item">
                       <a href="#"><?php echo "<img src ='data:image/jpeg;base64,".$newrowss["bimg1"]." ' ";?></a>
                        <h3><a href="#"><?php echo $newrowss['bname']; ?></a></h3>
                    </div>
                </div>
                <?php 
            }
            ?>
             
              </div>
          </div>
      </section>
												<!--//charts-->
												<center><h2>Percentage of Books Read</h2></center>  <br><br>
										<!--/bottom-grids-->		 
									<div class="bottom-grids">

										<div class="dev-table">  

											<div class="col-md-3 dev-col">                                    
	
                                            <div class="dev-widget dev-widget-transparent">
                                                <h2 class="inner one">Horror Books</h2>
                                                <p>The percentage of books read as per category</p>                                        
                                                <div class="dev-stat"><span class="counter"><?php echo $htotal; ?></span>%</div>                                                                                
                                                <div class="progress progress-bar-xs">
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo $htotal; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $htotal."%"?>;"></div>
                                                </div>                                        
                                               

                                                <a href="vpb.php?cat=horror" class="dev-drop">Take a closer look <span class="fa fa-angle-right pull-right"></span></a>
                                            </div>

                                        </div>
                                        <div class="col-md-3 dev-col mid">                                    

                                            <div class="dev-widget dev-widget-transparent dev-widget-success">
                                                 <h3 class="inner">Comedy Books</h3>
                                                <p>This The percentage of books read as per category</p>                                        
                                                <div class="dev-stat"><span class="counter"><?php echo $htotal."%"?></span></div>                                                                                
                                                <div class="progress progress-bar-xs">
                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $htotal."%"?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $htotal."%"?>;"></div>
                                                </div>                                        
                                               

                                                <a href="vpb.php?cat=comedy" class="dev-drop">Take a closer look <span class="fa fa-angle-right pull-right"></span></a>
                                            </div>

                                        </div>
                                        <div class="col-md-3 dev-col mid">                                    

										
                                            <div class="dev-widget dev-widget-transparent dev-widget-danger">
                                                 <h3 class="inner">Fantasy Books</h3>
                                                <p>The percentage of books read as per category</p>
                                                <div class="dev-stat"><span class="counter"><?php echo $ftotal."%"?></span></div>                                                                                
                                                <div class="progress progress-bar-xs">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $htotal."%"?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $ftotal."%"?>;"></div>
                                                </div>                                        
                                               

                                                <a href="vpb.php?cat=fantasy" class="dev-drop">Take a closer look <span class="fa fa-angle-right pull-right"></span></a>                                        
                                            </div>

                                        </div>
                                         <div class="col-md-3 dev-col ">                                    
	
											
                                            <div class="dev-widget dev-widget-transparent dev-widget-danger">
                                                 <h3 class="inner">Sci-Fi Books</h3>
                                                <p>The percentage of books read as per category</p>
                                                <div class="dev-stat"><span class="counter"><?php echo $stotal."%"?></span></div>                                                                                
                                                <div class="progress progress-bar-xs">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $stotal; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $stotal."%"?>;"></div>
                                                </div>                                        
                                                

                                                <a href="vpb.php?cat=Sci-fi" class="dev-drop">Take a closer look <span class="fa fa-angle-right pull-right"></span></a>                                        
                                            </div>

                                        </div>
										<div class="clearfix"></div>		
										
										</div>
										</div>
									<!--//bottom-grids-->
									
									</div>
									<!--/charts-inner-->
									</div>
										<!--//outer-wp-->
									</div>
									 <!--footer section start-->
										<footer>
										   <p> 2021. All Rights Reserved. BookStore</a></p>
										</footer>
									<!--footer section end-->
								</div>
							</div>
				<!--//content-inner-->
			<!--/sidebar-menu-->
				<div class="sidebar-menu">
					<header class="logo">
					<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="index.php"> <span id="logo"> <h1>Bookstore</h1></span> 
					<!--<img id="logo" src="" alt="Logo"/>--> 
				  </a> 
				</header>
			<div style="border-top:1px solid rgba(69, 74, 84, 0.7)"></div>
			<!--/down-->
							<div class="down">	
									  <a href="profile"><img src="https://static.thenounproject.com/png/17241-200.png" height="80" width="80"></a>
									  <a href="profile"><span class=" name-caret"><?php echo $name; ?></span></a>
									 <p>System Administrator in Company</p>
									<ul>
									<li><a class="tooltips" href="profile.php"><span>Profile</span><i class="lnr lnr-user"></i></a></li>
										<li><a class="tooltips" href="logout"><span>Log out</span><i class="lnr lnr-power-switch"></i></a></li>
										</ul>
									</div>
							   <!--//down-->
                           <div class="menu">
									<ul id="menu" >
									<li><a href="index
									"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
										 <li id="menu-academico" ><a href="#"> <i class="fa fa-book"></i> <span>Books</span> <span class="fa fa-angle-right" style="float: right"></span></a>
											 <ul id="menu-academico-sub" >
												<li id="menu-academico-avaliacoes" ><a href="vb.php">View / Delete Books</a></li>
												<li id="menu-academico-boletim" ><a href="ab.php">Add Books</a></li>
											  </ul>
										 </li>
									 <li><a href="#"><i class="lnr lnr-envelope"></i> <span>Mail Box</span><span class="fa fa-angle-right" style="float: right"></span></a>
									   <ul>
										<li><a href="inbox.php"><i class="fa fa-inbox"></i> Inbox</a></li>
										<li><a href="compose.php"><i class="fa fa-pencil-square-o"></i> Compose Mail</a></li>
									
									  </ul>
									</li>
								  </ul>
								</div>
							  </div>
							  <div class="clearfix"></div>		
							</div>
							<script>
							var toggle = true;
										
							$(".sidebar-icon").click(function() {                
							  if (toggle)
							  {
								$(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
								$("#menu span").css({"position":"absolute"});
							  }
							  else
							  {
								$(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
								setTimeout(function() {
								  $("#menu span").css({"position":"relative"});
								}, 400);
							  }
											
											toggle = !toggle;
										});
							</script>
<!--js -->
<link rel="stylesheet" href="css/vroom.css">
<script type="text/javascript" src="js/vroom.js"></script>
<script type="text/javascript" src="js/TweenLite.min.js"></script>
<script type="text/javascript" src="js/CSSPlugin.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.min.js"></script>
</body>
</html>