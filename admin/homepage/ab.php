<?php 
session_start();
$name = $_SESSION['username'];
include '../dbconn.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Bookstore : Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Augment Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/amcharts.js"></script>	
<script src="js/serial.js"></script>	
<script src="js/light.js"></script>	
<script src="js/radar.js"></script>	
<link href="css/barChart.css" rel='stylesheet' type='text/css' />
<link href="css/fabochart.css" rel='stylesheet' type='text/css' />
<!--clock init-->
<script src="js/css3clock.js"></script>
<!--Easy Pie Chart-->
<!--skycons-icons-->
<script src="js/skycons.js"></script>

<script src="js/jquery.easydropdown.js"></script>

<!--//skycons-icons-->
</head> 
<body>
   <div class="page-container">
   <!--/content-inner-->
	<div class="left-content">
	   <div class="inner-content">
		<!-- header-starts -->
			<div class="header-section">
						<!--menu-right-->
						<div class="top_menu">
						        <div class="main-search">
											<form>
											   <input type="text" value="Search" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Search';}" class="text"/>
												<input type="submit" value="">
											</form>
									<div class="close"><img src="images/cross.png" /></div>
								</div>
									<script type="text/javascript">
										 $('.main-search').hide();
										$('button').click(function (){
											$('.main-search').show();
											$('.main-search text').focus();
										}
										);
										$('.close').click(function(){
											$('.main-search').hide();
										});
									</script>
							<!--/profile_details-->
								<div class="profile_details_left">
									<ul class="nofitications-dropdown">
											<li class="dropdown note dra-down">
													
																		<script type="text/javascript">
			
																	function DropDown(el) {
																		this.dd = el;
																		this.placeholder = this.dd.children('span');
																		this.opts = this.dd.find('ul.dropdown > li');
																		this.val = '';
																		this.index = -1;
																		this.initEvents();
																	}
																	DropDown.prototype = {
																		initEvents : function() {
																			var obj = this;

																			obj.dd.on('click', function(event){
																				$(this).toggleClass('active');
																				return false;
																			});

																			obj.opts.on('click',function(){
																				var opt = $(this);
																				obj.val = opt.text();
																				obj.index = opt.index();
																				obj.placeholder.text(obj.val);
																			});
																		},
																		getValue : function() {
																			return this.val;
																		},
																		getIndex : function() {
																			return this.index;
																		}
																	}

																	$(function() {

																		var dd = new DropDown( $('#dd') );

																		$(document).click(function() {
																			// all dropdowns
																			$('.wrapper-dropdown-3').removeClass('active');
																		});

																	});

																</script>
										    </li>
									      <?php
												$sql = "SELECT * FROM contact order by ID DESC limit 3";
                                                           $res = mysqli_query($conn,$sql);
                                                           $count = mysqli_num_rows($res);

												?>
										<li class="dropdown note">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope-o"></i> <span class="badge"><?php echo $count; ?></span></a>
												<?php
function time_ago_in_php($timestamp){
  
  date_default_timezone_set("Asia/Kolkata");         
  $time_ago        = strtotime($timestamp);
  $current_time    = time();
  $time_difference = $current_time - $time_ago;
  $seconds         = $time_difference;
  
  $minutes = round($seconds / 60); 
  $hours   = round($seconds / 3600);
  $days    = round($seconds / 86400);   
  $weeks   = round($seconds / 604800);  
  $months  = round($seconds / 2629440);  
  $years   = round($seconds / 31553280); 
                
  if ($seconds <= 60){

    return "Just Now";

  } else if ($minutes <= 60){

    if ($minutes == 1){

      return "one minute ago";

    } else {

      return "$minutes minutes ago";

    }

  } else if ($hours <= 24){

    if ($hours == 1){

      return "an hour ago";

    } else {

      return "$hours hrs ago";

    }

  } else if ($days <= 7){

    if ($days == 1){

      return "yesterday";

    } else {

      return "$days days ago";

    }

  } else if ($weeks <= 4.3){

    if ($weeks == 1){

      return "a week ago";

    } else {

      return "$weeks weeks ago";

    }

  } else if ($months <= 12){

    if ($months == 1){

      return "a month ago";

    } else {

      return "$months months ago";

    }

  } else {
    
    if ($years == 1){

      return "one year ago";

    } else {

      return "$years years ago";

    }
  }
}
												?>
												<?php
												$sql = "SELECT * FROM contact order by ID DESC limit 3";
                                                           $res = mysqli_query($conn,$sql);
                                                           $count = mysqli_num_rows($res);

												?>
													<ul class="dropdown-menu two first">
														<li>
															<div class="notification_header">
																<h3>You have <?php echo $count;?> new messages  </h3> 
															</div>
														</li>
														<?php 
                                                           
                                                           while($row = mysqli_fetch_assoc($res)) {
                                                           	$time = $row['dates'];
                                                           	?>
                                                           	<li><a href="#">
														   <div class="user_img"><img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png" alt=""></div>
														   <div class="notification_desc">
															<p><?php echo $row['name']; ?> Contacted You</p>
															<p><span><?php echo time_ago_in_php($time); ?></span></p>
															</div>
														   <div class="clearfix"></div>	
														 </a></li>
                                                           	<?php
                                                           }
														?>
														
														
														 
															<div class="notification_bottom">
																<a href="inbox.php">See all messages</a>
															</div> 
														</li>
													</ul>
										</li>
										<?php
$asql = "SELECT * FROM users order by ID DESC limit 5";
$ares = mysqli_query($conn,$asql);
$count = mysqli_num_rows($ares);
?>
							<li class="dropdown note">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell-o"></i> <span class="badge"><?php echo $count; ?></span></a>

									<ul class="dropdown-menu two">
										<li>
											<div class="notification_header">
												<h3>You have <?php echo $count; ?> new user on BookStore</h3>
											</div>
										</li>
										<?php
										while($arow = mysqli_fetch_assoc($ares)) {
?>
<li><a href="#">
											<div class="user_img"><img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png" alt=""></div>
										   <div class="notification_desc">
											<p><?php echo $arow['uname']; ?> Has Registered</p>
											</div>
										  <div class="clearfix"></div>	
										 </a></li>
<?php
}
										?>
										
										
									</ul>
							</li>	
							<li class="dropdown note">
								<a href="../../login.php" class="dropdown-toggle"  aria-expanded="false"><i class="fa fa-home"></i> </a>
							</li>
									 							   		
							<div class="clearfix"></div>	
								</ul>
							</div>
							<div class="clearfix"></div>	
							<!--//profile_details-->
						</div>
						<!--//menu-right-->
					<div class="clearfix"></div>
				</div>
					<!-- //header-ends -->
					
			<!--/sidebar-menu-->
					<div class="sidebar-menu">
					<header class="logo">
					<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="index.php"> <span id="logo"> <h1>Bookstore</h1></span> 
					<!--<img id="logo" src="" alt="Logo"/>--> 
				  </a> 
				</header>
			<div style="border-top:1px solid rgba(69, 74, 84, 0.7)"></div>
			<!--/down-->
							<div class="down">	
									  <a href="profile"><img src="https://static.thenounproject.com/png/17241-200.png" height="80" width="80"></a>
									  <a href="profile"><span class=" name-caret"><?php echo $name; ?></span></a>
									 <p>System Administrator in Company</p>
									<ul>
									<li><a class="tooltips" href="profile.php"><span>Profile</span><i class="lnr lnr-user"></i></a></li>
										<li><a class="tooltips" href="logout"><span>Log out</span><i class="lnr lnr-power-switch"></i></a></li>
										</ul>
									</div>
							   <!--//down-->
                           <div class="menu">
									<ul id="menu" >
									<li><a href="index"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
										 <li id="menu-academico" ><a href="#"> <i class="fa fa-book"></i> <span>Books</span> <span class="fa fa-angle-right" style="float: right"></span></a>
											 <ul id="menu-academico-sub" >
												<li id="menu-academico-avaliacoes" ><a href="vb.php">View / Delete Books</a></li>
												<li id="menu-academico-boletim" ><a href="ab.php">Add Books</a></li>
											  </ul>
										 </li>
									 <li><a href="#"><i class="lnr lnr-envelope"></i> <span>Mail Box</span><span class="fa fa-angle-right" style="float: right"></span></a>
									   <ul>
										<li><a href="inbox.php"><i class="fa fa-inbox"></i> Inbox</a></li>
										<li><a href="compose.php"><i class="fa fa-pencil-square-o"></i> Compose Mail</a></li>
									
									  </ul>
									</li>
								  </ul>
								</div>
							  </div>
							  <div class="clearfix"></div>		
							</div>
							<script>
							var toggle = true;
										
							$(".sidebar-icon").click(function() {                
							  if (toggle)
							  {
								$(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
								$("#menu span").css({"position":"absolute"});
							  }
							  else
							  {
								$(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
								setTimeout(function() {
								  $("#menu span").css({"position":"relative"});
								}, 400);
							  }
											
											toggle = !toggle;
										});
							</script>
<!--js -->
<link rel="stylesheet" href="css/vroom.css">
<script type="text/javascript" src="js/vroom.js"></script>
<script type="text/javascript" src="js/TweenLite.min.js"></script>
<script type="text/javascript" src="js/CSSPlugin.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.min.js"></script>
 <div class="outter-wp">
								<!--custom-widgets-->
												<div class="custom-widgets">
												   <form action="abp.php" method="POST" enctype="multipart/form-data">
												   	 <label>Book Name :-</label>&nbsp;&nbsp;&nbsp;<input type = "text" placeholder = "Enter Book Name" value = "" required name = "bname"><br>
    <label>Author Name :-</label>&nbsp;&nbsp;&nbsp;<input type = "text" placeholder = "Enter Author Name" required name = "authname"><br><br>
    <label>Book Category:-</label>&nbsp;&nbsp;&nbsp;
    <select name = "btype">
      <option value = "Sci-fi">Sci-fi</option>
      <option value = "horror">horror</option>
      <option value = "comedy">comedy</option>
      <option value = "fantasy">fantasy</option>
  </select><br><br>
  <label>Book Description :-</label><br><textarea name="bdesc" placeholder="Enter Book Description"></textarea><br>
  <label>Book Image #1 :- </label> <input type="file" name="bimg1">
  <label>Book PDF :- </label> <input type="file" name="pdf">
 
 <style type="text/css">
 	.outter-wp .custom-widgets form {
 		height:auto;
 		width: 100%;
 		float:left;
 		color:black;
 	}
 	.outter-wp .custom-widgets form input {
 		height: 35px;
 		width: 600px;
 		padding-left:5px;
 		border-radius: 1px;
 		border:none;
 		background-color: transparent;
 		border-bottom: 1px solid black;
 		border-radius: 0px;
 		color: black;
 		margin-top:10px;
 	}
 	.outter-wp .custom-widgets form input[type = submit] {
 		height: 45px;
 		width: 90%;
 		padding-left:5px;
 		border-radius: 1px;
 		border:none;
 		background-color: black;
 		color: white;
 		margin-top:10px;
 		border-radius: 5px;
 		font-weight: bold;
 	}
 	.outter-wp .custom-widgets form input[type = submit]:hover {
 		background-color:white;
 		color:black;
 		border:1px solid black;
 		transition: 0.5s ease;
 	}
 	.outter-wp .custom-widgets form textarea {
 		height: 350px;
 		width: 600px;
 		padding-left:5px;
 		border-radius: 5px;
 		background-color: white;
 		color: black;
 		margin-top:10px;
 	}
 </style>
    <input type = "submit" value = "Submit"> <br><br>
												   </form>

												</div>
												<!--//custom-widgets-->
												<!--/candile-->
												
													<!--/candile-->
													
												<!--/charts-->
												
										<!--//outer-wp-->
									</div>
</body>
</html>