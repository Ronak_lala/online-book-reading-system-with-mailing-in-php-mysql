<?php
include('dbconn.php');

?>
<?php
session_start();
$name = $_SESSION['uname'];
if(!isset($name)) {
    header("location:login");
}
$newquery = "SELECT * FROM users WHERE uname = '$name'";
$result = mysqli_query($conn,$newquery);
$rrows = mysqli_fetch_assoc($result);
$uid = $rrows['ID'];
?>
 <!DOCTYPE html>

<html>
<head>
	 <meta charset="UTF-8">
    <title>Book Store</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#03a6f3">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/styles.css">
    <style type="text/css">
                        .btn {
                            border:none;background-color: white;color: red;height: 40px;
                         width: 170px;line-height: 0px;font-size: 12px;text-align: center;margin-left: 0px;border:1px solid black;
                         border-radius: 5px;
                        }
                           .btn:hover {
                            background-color: red;
                            color: white;
                           }
                       </style>
</head>
<body>
	 <header>
        <div class="main-menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="navbar-item active">
                                <a href="index.php" class="nav-link">Home</a>
                            </li>
                            <li class="navbar-item">
                                <a href="books.php" class="nav-link">Read</a>
                            </li>
                            <li class="navbar-item">
                                <a href="about.php" class="nav-link">About</a>
                            </li>
                            <li class="navbar-item">
                                <a href="profile.php" class="nav-link"><?php echo $_SESSION['uname'];?></a>
                            </li>
                            <li class="navbar-item">
                                <a href="logout.php" class="nav-link">Logout</a>
                            </li>
                            
                        </ul>
                        <?php
                        $uid = $rrows['ID'];
                          $sql = "SELECT * FROM bookmark WHERE uid = '$uid'";
                          $result = mysqli_query($conn,$sql);
                          $rows = mysqli_num_rows($result);
                        ?>
                        <a href = "ubm.php"><div class="cart my-2 my-lg-0">
                            <span>
                                <i class="fa fa-bookmark-o" aria-hidden="true"></i></span>
                            <span class="quntity"><?php echo $rows; ?></span>
                        </div></a>
                        <form class="form-inline my-2 my-lg-0" action="search.php" method="POST">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search here..." aria-label="Search" name = "search" required>
                            <input type="submit" value = "🔍" style="background-color: white;">
                        </form>
                           
                    </div>
                </nav>
            </div>
        </div>
    </header>
      <section class="recent-book-sec">
        <div class="container">
            <div class="title">
                <h2>BookMarked Books</h2>
                <hr>
            </div>
            <div class="row">

            	<?php 
            	include('dbconn.php');
                $sql = "SELECT * FROM bookmark WHERE uid = '{$uid}'";
                $res = mysqli_query($conn,$sql);
                while($row = mysqli_fetch_assoc($res)) {
                    $bid = $row['bid']; 
                    $newsql = "SELECT * FROM books where ID = '{$bid}'";
                    $newres = mysqli_query($conn,$newsql);
                    $newrow = mysqli_fetch_assoc($newres);
                	?>

                	 <a href = "product-single.php?id=<?php echo $newrow['ID']?>&&btype=<?php echo $newrow['btype']?>">
                        <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="item">
                        <?php echo "<img src ='data:image/jpeg;base64,".$newrow["bimg1"]." ' ";?> 
                       
                        <center><h3><a style="color:black;" href="product-single.php?id=<?php echo $newrow['ID']?>&&btype=<?php echo $newrow['btype']?>"><?php echo $newrow['bname'] ?></a></h3><br></center>
                         <a href = "dbm.php?id=<?php echo $row['ID']?>"><button class="btn" id="remove">Remove</button></a>
                           
                    </div>
                    
                </div></a>
                	<?php
                }
            	?>
             
                       
                
            </div>
            
        </div>
    </section>

</body>
</html>