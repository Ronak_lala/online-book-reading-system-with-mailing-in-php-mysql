<?php
include('dbconn.php');
?>
<?php
session_start();
$name = $_SESSION['uname'];
$newquery = "SELECT * FROM users WHERE uname = '$name'";
$result = mysqli_query($conn,$newquery);
$rrows = mysqli_fetch_assoc($result);
?>
 <!DOCTYPE html>
<html>
<head>
	 <meta charset="UTF-8">
    <title>Book Store</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#03a6f3">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
	 <header>
        <div class="main-menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="navbar-item active">
                                <a href="index.php" class="nav-link">Home</a>
                            </li>
                            <li class="navbar-item">
                                <a href="books.php" class="nav-link">Read</a>
                            </li>
                            <li class="navbar-item">
                                <a href="about.php" class="nav-link">About</a>
                            </li>
                            <li class="navbar-item">
                                <a href="profile.php" class="nav-link"><?php echo $_SESSION['uname'];?></a>
                            </li>
                            <li class="navbar-item">
                                <a href="logout.php" class="nav-link">Logout</a>
                            </li>
                            
                        </ul>
                        <?php
                        $uid = $rrows['ID'];
                          $sql = "SELECT * FROM bookmark WHERE uid = '$uid'";
                          $result = mysqli_query($conn,$sql);
                          $rows = mysqli_num_rows($result);
                        ?>
                        <a href = "ubm.php"><div class="cart my-2 my-lg-0">
                            <span>
                                <i class="fa fa-bookmark-o" aria-hidden="true"></i></span>
                            <span class="quntity"><?php echo $rows; ?></span>
                        </div></a>
                        <form class="form-inline my-2 my-lg-0" action="search">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search here..." aria-label="Search" name = "search" required>
                            <input type="submit" value = "🔍">
                        </form>
                           
                    </div>
                </nav>
            </div>
        </div>
    </header>
      <section class="recent-book-sec">
        <div class="container">
            <div class="title">
                <h2>New-Arrivals in BookStore</h2>
                <hr>
            </div>
            <div class="row">
            	<?php 
            	include('dbconn.php');
                $sql = "SELECT * FROM books order by ID DESC limit 10";
                $res = mysqli_query($conn,$sql);
                while($row = mysqli_fetch_assoc($res)) {
                	?>
                	 <a href = "product-single.php?id=<?php echo $row['ID']?>&&btype=<?php echo $row['btype']?>">
                        <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="item">
                        <?php echo "<img src ='data:image/jpeg;base64,".$row["bimg1"]." ' ";?>
                        <h3><a href="product-single.php?id=<?php echo $row['ID']?>&&btype=<?php echo $row['btype']?>"><?php echo $row['bname'] ?></a></h3>
                        <h6><a href="product-single.php?id=<?php echo $row['ID']?>&&btype=<?php echo $row['btype']?>">Read Now</a></h6>
                    </div>
                </div></a>
                	<?php
                }
            	?>
               
                
            </div>
            
        </div>
    </section>

</body>
</html>