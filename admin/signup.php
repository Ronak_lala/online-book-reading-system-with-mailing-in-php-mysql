<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Style -->
    <link rel="stylesheet" href="css/style.css">
<script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <title>Signup Admin</title>
  </head>
  <body>
  

  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-6 order-md-2">
          <img src="images/undraw_file_sync_ot38.svg" alt="Image" class="img-fluid">
        </div>
        <div class="col-md-6 contents">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="mb-4">
              <h3>Sign up to <strong>Admin</strong></h3>
              <p class="mb-4"><!-- Sample text -->.</p>
            </div>
           <div class="form-group first">
                <label for="uname"></label>
                <input type="text" class="form-control" id="name" placeholder="Name">

              </div>
              <div class="form-group first">
                <label for="username"></label>
                <input type="text" class="form-control" id="username" placeholder="Username">

              </div>
               <div class="form-group first">
                <label for="email"></label>
                <input type="email" class="form-control" id="email" placeholder="Email">

              </div>
               <div class="form-group first">
                <label for="phno"></label>
                <input type="number" class="form-control" id="phno" placeholder="Phone Number">

              </div>
               <div class="form-group first">
                <label for="password"></label>
                <input type="password" class="form-control" id="password" placeholder="Password">

              </div>
              <div class="form-group last mb-4">
                <label for="cpassword"></label>
                <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password">
                
              </div>
              

              <input type="submit" value="Sign In" id = "signin" class="btn text-white btn-block btn-primary">
              <script type="text/javascript">
                 $("#signin").on("click",function() {
                  var name = $("#name").val();
                    var username = $("#username").val();
                    var email = $("#email").val();
                    var phno = $("#phno").val();
                    var pass = $("#password").val();
                    var cpass = $("#cpassword").val();
                    if(username == "" || email ==  "" || phno == "" || pass == "" || cpass == "" || name == "") {
                      swal("Error", "Fill the Entire form to Register", "error");
                    }
                    else if(pass != cpass){
                      swal("Error", "Passwords do not match", "error");
                    }
                    else {
                      $.ajax({
                          url:"sign.php",
                          type:"POST",
                          data:{username:username,email:email,phno:phno,pass:pass,cpass:cpass,name:name},
                          success:function(data){
                            if(data == 1) {
                               swal("Success", "Admin Registeration Done", "success").then(function() {
    window.location = "index.php";
});

                            }
                            else if(data == 2) {
                              swal("Error", "Oops! Username Already Taken", "error");

                            }
                            else {
swal("Error", "Internel Server Error! Please Try again Later", "error");
                            }
                          }
                      });
                    }
                 });
              </script>

              <span class="d-block text-left my-4 text-muted"><a href = "index">or LogIn here</a></span>
              
        
            </div>
          </div>
          
        </div>
        
      </div>
    </div>
  </div>

  
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>