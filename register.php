<?php
error_reporting(0);
@ini_set('display_errors', 0);
session_start();
$name = $_SESSION['uname'];

if(isset($name)) {
    header("location:index.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Book Store</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#03a6f3">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>

<body>
    <header>
        
        <div class="main-menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="navbar-item">
                                <a href="index.html" class="nav-link">Home</a>
                            </li>
                            <li class="navbar-item">
                                <a href="books.php" class="nav-link">Read</a>
                            </li>
                            <li class="navbar-item">
                                <a href="about" class="nav-link">About</a>
                            </li>
                            <li class="navbar-item active">
                                <a href="login.php" class="nav-link">Login / Signup</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <div class="breadcrumb">
        <div class="container">
            <a class="breadcrumb-item" href="index.html">Home</a>
            <span class="breadcrumb-item active">Register</span>
        </div>
    </div>
    <section class="static about-sec">
        <div class="container">
            <h1>My Account / REgister</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's printer took a galley of type and scrambled it to make a type specimen book. It has survived not only fiveLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem </p>
            <div class="form">
                
                    <div class="row">
                        <div class="col-md-12">
                            <input type="email" placeholder="Enter Your email Address" name="email" id = "email">
                            <span class="required-star">*</span>
                        </div>
                          <div class="col-md-6">
                            <input type="text" placeholder="Enter your full name" name="fname" id = "fname">
                            <span class="required-star">*</span>
                        </div>
                        <div class="col-md-6">
                            <input type="number" placeholder="Mobile number" name="phno" id = "phno">
                            <span class="required-star">*</span>
                        </div>
                        <div class="col-md-4">
                            <input placeholder="Enter User Name" name="uname" id = "uname">
                            <span class="required-star">*</span>
                        </div>
                        
                        <div class="col-md-4">
                            <input type="password" placeholder="Password" name="pass" id = "pass">
                            <span class="required-star">*</span>
                        </div>
                        <div class="col-md-4">
                            <input type="password" placeholder="Repeat Password" name="cpass" id = "cpass">
                            <span class="required-star">*</span>
                        </div>
                        
                        <div class="col-lg-8 col-md-12">
                            <button class="btn black" id = "register">Register</button>
                            <h5>not Registered? <a href="login.php">Login here</a></h5>
                        </div>
                    </div>
                
<script type="text/javascript">
    $("#register").on("click",function (){
        var uname = $("#uname").val();
        var pass = $("#pass").val();
        var cpass = $("#cpass").val();
        var email = $("#email").val();
        var phno = $("#phno").val();
        var fname = $("#fname").val();
        if(uname == '' || pass == '' || cpass == '' || email == '' || phno == '' || fname == '') {
            swal("Error","Please fill the form to register","error");
        }
        else if(pass != cpass) {
        swal("Error","Passwords do not match","error");
        }
        else {
            $.ajax({
               url:"reg.php",
               type:"POST",
               data:{uname:uname,pass:pass,cpass:cpass,email:email,phno:phno,fname:fname},
               success:function(data) {
                if(data == 1) {
 swal("Woohoo","You have registered successfully","success").then(function() {
    window.location = "login.php";
});
                }
                else if(data == 2) {
                    swal("Error","Username Already Taken","error");
                }
                else if (data == 3) {
                     swal("Error","Enter Valid Email Adrress","error");
                }
                else {
                     swal("Error",data,"error");
                }
               }

            });
        }
    });
</script>
            </div>
        </div>
    </section>
     <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="address">
                        <h4>Our Address</h4>
                        <h6>flat no.204,Aravalli building,Dipti sky city</h6>
                        <h6>Call : 9511962448</h6>
                        <h6>Email : infobookstore3@gmail.com</h6>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="navigation">
                        <h4>Navigation</h4>
                        <ul>
                            <li><a href="index">Home</a></li>
                            <li><a href="about">About Us</a></li>
                            <li><a href="privacy-policy">Privacy Policy</a></li>
                            <li><a href="terms-conditions">Terms</a></li>
                        </ul>
                    </div>
                    <div class="navigation">
                        <h4>Help</h4>
                        <ul>
                            <li><a href="privacy-policy">Privacy</a></li>
                            <li><a href="faq">FAQ’s</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form">
                        <h3>Quick Contact us</h3>
                        <h6>If you have any problem, you can leave a message</h6>
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <input placeholder="Name" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="email" placeholder="Email" required>
                                </div>
                                <div class="col-md-12">
                                    <textarea placeholder="Messege"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn black">Alright, Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h5>(C) 2021. All Rights Reserved. BookStore</h5>
                    </div>
                    <div class="col-md-6">
                        <div class="share align-middle">
                            <a href="https://www.facebook.com/abhishek.nagari.3/"><span class="fb"><i class="fa fa-facebook-official"></i></span></a>
                           <a href="https://www.instagram.com/mr._.awesome._/"><span class="instagram"><i class="fa fa-instagram"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>

